package fr.open.kierann.security.authorization;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthorizationException extends ResponseStatusException {
    public AuthorizationException(String message) {
        super(HttpStatus.FORBIDDEN, message);
    }
}

package fr.open.kierann.security.authorization;

import org.springframework.stereotype.Component;

@Component
public class AuthorizationManager {
    public void authorize(Object authenticatedUser, Authorize authorizeAnnotation) {
        if (authorizeAnnotation == null) {
            return;
        }
        if (authenticatedUser == null) {
            throw new AuthorizationException(authorizeAnnotation.message());
        }

        Class<?> authenticatedUserClass = authenticatedUser.getClass();
        for (Class<?> authorizedUserClass : authorizeAnnotation.value()) {
            if (authorizedUserClass.isAssignableFrom(authenticatedUserClass)) {
                return;
            }
        }
        throw new AuthorizationException(authorizeAnnotation.message());
    }
}

package fr.open.kierann.security.authentication;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RequiredArgsConstructor
@Component
public class AuthenticationManager {
    private final UserResolver userResolver;

    private final Random random = new SecureRandom();

    private final ConcurrentMap<String, Authentication> authentications = new ConcurrentHashMap<>();

    @Value("${security.authentication.token.length}")
    private Integer authenticationTokenLength;

    @Value("${security.authentication.timeout}")
    private Duration authenticationTimeout;

    @Value("${security.authentication.revocation.timeout}")
    private Duration authenticationRevocationTimeout;

    @Value("${security.authentication.capacity}")
    private Integer authenticationCapacity;

    @Value("${security.authentication.cleanup.min-interval}")
    private Duration authenticationCleanupMinInterval;

    @Value("${security.authentication.saturation.message:Too many users are currently connected. Please try again in a few minutes}")
    private String authenticationSaturationMessage;

    @Value("${security.invalid-authentication-token.message:Your fr.open.security.authentication token is invalid. Please log in and try again.}")
    private String invalidAuthenticationTokenMessage;

    @Value("${security.expired-authentication.message:Your session has expired. Please log in and try again.}")
    private String expiredAuthenticationMessage;

    private LocalDateTime lastAuthenticationCleanup = LocalDateTime.now(ZoneOffset.UTC);

    public Object getAuthenticatedUser(@Nullable String authenticationToken) {
        Authentication authentication = validateAuthenticationToken(authenticationToken);
        Object userId = authentication.getUserId();
        return userResolver.getUser(userId);
    }

    @PackagePrivate
    String authenticate(Object user) {
        if (LocalDateTime.now(ZoneOffset.UTC).isAfter(lastAuthenticationCleanup.plus(authenticationCleanupMinInterval))) {
            removeExpiredAuthentications();
        }

        if (authentications.size() >= authenticationCapacity) {
            throw new SaturationException(authenticationSaturationMessage);
        }

        String authenticationToken = generateNewAuthenticationToken();
        Object userId = userResolver.getUserId(user);
        Authentication authentication = new Authentication(userId, authenticationTimeout);
        authentications.put(authenticationToken, authentication);
        return authenticationToken;
    }

    @PackagePrivate
    String renewAuthentication(@Nullable String authenticationToken) {
        Authentication currentAuthentication = validateAuthenticationToken(authenticationToken);
        if (currentAuthentication.isRevoked()) {
            throw new AuthenticationException(invalidAuthenticationTokenMessage);
        }
        currentAuthentication.revoke(authenticationRevocationTimeout);

        String renewedAuthenticationToken = generateNewAuthenticationToken();
        Object userId = currentAuthentication.getUserId();
        Authentication renewedAuthentication = new Authentication(userId, authenticationTimeout);
        authentications.put(renewedAuthenticationToken, renewedAuthentication);
        return renewedAuthenticationToken;
    }

    @PackagePrivate
    void revokeAuthentication(@Nullable String authenticationToken) {
        if (authenticationToken != null) {
            authentications.remove(authenticationToken);
        }
    }

    private Authentication validateAuthenticationToken(@Nullable String authenticationToken) {
        if (authenticationToken == null) {
            throw new AuthenticationException(invalidAuthenticationTokenMessage);
        }
        Authentication authentication = authentications.get(authenticationToken);

        if (authentication == null) {
            throw new AuthenticationException(invalidAuthenticationTokenMessage);
        }

        if (authentication.hasExpired()) {
            throw new AuthenticationException(expiredAuthenticationMessage);
        }

        return authentication;
    }

    private String generateNewAuthenticationToken() {
        String authenticationToken;
        do {
            authenticationToken = RandomStringUtils.random(authenticationTokenLength, 0, 0, true, true, null, random);
        } while (authentications.containsKey(authenticationToken));
        return authenticationToken;
    }

    private void removeExpiredAuthentications() {
        authentications.entrySet().removeIf(entry -> entry.getValue().hasExpired());
        lastAuthenticationCleanup = LocalDateTime.now(ZoneOffset.UTC);
    }

    private static class Authentication {
        @Getter
        private final Object userId;

        @Getter
        private boolean revoked = false;

        private LocalDateTime expirationTimestamp;

        public Authentication(Object userId, Duration timeout) {
            this.userId = userId;
            expirationTimestamp = LocalDateTime.now(ZoneOffset.UTC).plus(timeout);
        }

        public boolean hasExpired() {
            return LocalDateTime.now(ZoneOffset.UTC).isAfter(expirationTimestamp);
        }

        public void revoke(Duration revocationTimeout) {
            revoked = true;
            expirationTimestamp = LocalDateTime.now(ZoneOffset.UTC).plus(revocationTimeout);
        }
    }
}

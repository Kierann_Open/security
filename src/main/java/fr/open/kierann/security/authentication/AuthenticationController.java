package fr.open.kierann.security.authentication;

import lombok.RequiredArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@PackagePrivate
@RequestMapping("${security.authentication.path:authentication}")
@RestController
class AuthenticationController {
    private final AuthenticationContext<Object> authenticationContext;

    private final AuthenticationManager authenticationManager;

    private final CredentialsValidator credentialsValidator;

    @PostMapping("${security.authentication.log-in.mapping:logIn}")
    public ResponseEntity<Object> handleLogIn(
            @Nullable @RequestParam(value = "${security.authentication.identifier.parameter.name:identifier}", required = false) String identifier,
            @Nullable @RequestParam(value = "${security.authentication.raw-password.parameter.name:raw-password}", required = false)
            String rawPassword) {
        Object user = credentialsValidator.validate(identifier, rawPassword);
        String authenticationToken = authenticationManager.authenticate(user);
        return addAuthenticationToken(ResponseEntity.ok(), authenticationToken).body(user);
    }

    @GetMapping("${security.authentication.get.mapping:}")
    public Object handleGetAuthenticatedUser() {
        return authenticationContext.getUser();
    }

    @PostMapping("${security.authentication.renewal.mapping:renew}")
    public ResponseEntity<Void> handleAuthenticationRenewal(RequestEntity<Void> request) {
        String authenticationToken = getAuthenticationToken(request);
        String renewedAuthenticationToken = authenticationManager.renewAuthentication(authenticationToken);
        return addAuthenticationToken(ResponseEntity.ok(), renewedAuthenticationToken).build();
    }

    @PostMapping("${security.authentication.log-out.mapping:logOut}")
    public ResponseEntity<Void> handleLogOut(RequestEntity<Void> request) {
        String authenticationToken = getAuthenticationToken(request);
        authenticationManager.revokeAuthentication(authenticationToken);
        return addAuthenticationToken(ResponseEntity.ok(), "").build();
    }

    private <B extends ResponseEntity.HeadersBuilder<B>> B addAuthenticationToken(ResponseEntity.HeadersBuilder<B> builder,
                                                                                  String authenticationToken) {
        return builder.header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER)
                      .header(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER, authenticationToken);
    }

    @Nullable
    private String getAuthenticationToken(RequestEntity<?> request) {
        return request.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
    }
}

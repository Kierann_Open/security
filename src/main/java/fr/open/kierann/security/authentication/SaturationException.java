package fr.open.kierann.security.authentication;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class SaturationException extends ResponseStatusException {
    public SaturationException(String message) {
        super(HttpStatus.SERVICE_UNAVAILABLE, message);
    }
}

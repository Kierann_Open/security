package fr.open.kierann.security.authentication;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceUnitUtil;
import lombok.RequiredArgsConstructor;
import lombok.experimental.PackagePrivate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@PackagePrivate
@Slf4j
@Component
class UserResolver implements InitializingBean {
    private final AuthenticationConfiguration configuration;

    private final EntityManager entityManager;

    private PersistenceUnitUtil persistenceUnitUtil;

    private Class<?> userEntity;

    @Override
    public void afterPropertiesSet() {
        persistenceUnitUtil = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        userEntity = configuration.getUserEntity();
    }

    public Object getUserId(Object user) {
        return persistenceUnitUtil.getIdentifier(user);
    }

    public Object getUser(Object userId) {
        Object user = entityManager.find(userEntity, userId);
        if (user == null) {
            throw new IllegalArgumentException("There is no user with id #" + userId);
        }
        return user;
    }
}

package fr.open.kierann.security.authentication;

import jakarta.persistence.EntityManager;
import jakarta.persistence.metamodel.EntityType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationConfigurationException;

import java.lang.reflect.Field;
import java.util.Set;

@RequiredArgsConstructor
@Configuration
public class AuthenticationConfiguration implements InitializingBean {

    public static final String AUTHENTICATION_TOKEN_HEADER = "Authentication-Token";

    private final EntityManager entityManager;

    @Getter
    private Class<?> userEntity;

    @Getter
    private Field userEntityIdentifierField;

    @Getter
    private Field userEntityPasswordField;

    @Override
    public void afterPropertiesSet() {
        Set<EntityType<?>> entities = entityManager.getMetamodel().getEntities();
        for (EntityType<?> entity : entities) {
            Class<?> entityClass = entity.getJavaType();
            if (entityClass.isAnnotationPresent(UserEntity.class)) {
                registerUserEntityClass(entityClass);
                break;
            }
        }

        if (userEntity == null) {
            throw new BeanInitializationException(
                    "No user entity was found. To register the user entity, annotate it with '@" + UserEntity.class.getSimpleName() + "'");
        }
    }

    public void registerUserEntityClass(Class<?> userEntity) {
        UserEntity userAnnotation = userEntity.getAnnotation(UserEntity.class);
        if (userAnnotation == null) {
            throw new AnnotationConfigurationException(
                    "The user entity class '" + userEntity.getName() + "' must be annotated with '@" + UserEntity.class.getSimpleName() + "'");
        }

        this.userEntity = userEntity;

        try {
            userEntityIdentifierField = userEntity.getDeclaredField(userAnnotation.identifier());
            userEntityPasswordField = userEntity.getDeclaredField(userAnnotation.password());
        } catch (NoSuchFieldException exception) {
            throw new AnnotationConfigurationException("Invalid user entity configuration", exception);
        }

        userEntityPasswordField.setAccessible(true);
    }
}

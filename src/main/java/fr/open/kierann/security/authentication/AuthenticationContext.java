package fr.open.kierann.security.authentication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Optional;

@Component
@RequestScope
public class AuthenticationContext<T> {
    private T user = null;

    @Value("${security.not-authenticated.message:You are not authenticated. Please log in and try again.}")
    private String notAuthenticatedMessage;

    public Optional<T> findUser() {
        return Optional.ofNullable(user);
    }

    public T getUser() {
        if (user == null) {
            throw new AuthenticationException(notAuthenticatedMessage);
        }
        return user;
    }

    public void setUser(T authenticatedUser) {
        if (user != null) {
            throw new IllegalStateException("An user is already registered in the fr.open.security.authentication context for this request");
        }
        user = authenticatedUser;
    }
}

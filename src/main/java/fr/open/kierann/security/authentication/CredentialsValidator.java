package fr.open.kierann.security.authentication;

import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Query;
import lombok.RequiredArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

@RequiredArgsConstructor
@PackagePrivate
@Component
class CredentialsValidator implements InitializingBean {
    private final AuthenticationConfiguration configuration;

    private final EntityManager entityManager;

    private final PasswordEncoder passwordEncoder;

    @Value("${security.invalid-credentials.message:Invalid credentials}")
    private String invalidCredentialsMessage;

    private AuthenticationException authenticationException;

    private Class<?> userEntity;

    private String userQueryString;

    @Override
    public void afterPropertiesSet() {
        authenticationException = new AuthenticationException(invalidCredentialsMessage);
        userEntity = configuration.getUserEntity();
        String userEntityName = userEntity.getName();
        String userEntityIdentifierFieldName = configuration.getUserEntityIdentifierField().getName();
        userQueryString = "from " + userEntityName + " where " + userEntityIdentifierFieldName + " = :identifier";
    }

    public Object validate(@Nullable String identifier, @Nullable String rawPassword) {
        if (identifier == null || rawPassword == null) {
            throw authenticationException;
        }

        Object user = findUserFromIdentifier(identifier);
        validatePassword(rawPassword, user);

        return user;
    }

    private Object findUserFromIdentifier(String userCredentialsIdentifier) {
        Query userQuery = entityManager.createQuery(userQueryString, userEntity).setParameter("identifier", userCredentialsIdentifier);

        try {
            return userQuery.getSingleResult();
        } catch (NoResultException ignored) {
            throw authenticationException;
        }
    }

    private void validatePassword(String userCredentialsPassword, Object user) {
        Field userEntityPasswordField = configuration.getUserEntityPasswordField();
        String userEntityPassword = (String) ReflectionUtils.getField(userEntityPasswordField, user);
        if (!passwordEncoder.matches(userCredentialsPassword, userEntityPassword)) {
            throw authenticationException;
        }
    }
}

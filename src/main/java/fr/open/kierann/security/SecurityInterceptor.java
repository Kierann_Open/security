package fr.open.kierann.security;

import fr.open.kierann.security.authentication.AuthenticationConfiguration;
import fr.open.kierann.security.authentication.AuthenticationContext;
import fr.open.kierann.security.authentication.AuthenticationException;
import fr.open.kierann.security.authentication.AuthenticationManager;
import fr.open.kierann.security.authorization.AuthorizationManager;
import fr.open.kierann.security.authorization.Authorize;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RequiredArgsConstructor
@PackagePrivate
@Component
class SecurityInterceptor implements HandlerInterceptor, WebMvcConfigurer {
    private final AuthenticationManager authenticationManager;

    private final AuthorizationManager authorizationManager;

    private final AuthenticationContext<Object> authenticationContext;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String authenticationToken = request.getHeader(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        Object authenticatedUser = handleAuthentication(authenticationToken);

        handleAuthorization(authenticatedUser, handler);

        return true;
    }

    private Object handleAuthentication(String authenticationToken) {
        try {
            Object authenticatedUser = authenticationManager.getAuthenticatedUser(authenticationToken);
            authenticationContext.setUser(authenticatedUser);
            return authenticatedUser;
        } catch (AuthenticationException ignored) {
            return null;
        }
    }

    private void handleAuthorization(Object authenticatedUser, Object handler) {
        if (!(handler instanceof HandlerMethod)) {
            return;
        }
        Authorize authorize = ((HandlerMethod) handler).getMethodAnnotation(Authorize.class);
        authorizationManager.authorize(authenticatedUser, authorize);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this).excludePathPatterns("/error/**");
    }
}

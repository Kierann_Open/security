package integration.context.endpoint;

import fr.open.kierann.security.authorization.Authorize;
import integration.context.authentication.TestUser;
import integration.context.authorization.HyperUserA;
import integration.context.authorization.SuperUserA;
import integration.context.authorization.SuperUserB;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("fr.open.security.authorization-test")
@RestController
public class AuthorizationTestController {
    @GetMapping("public")
    public void publicRequest() {
    }

    @Authorize(value = TestUser.class, message = "Users only")
    @GetMapping("user")
    public void userRequest() {
    }

    @Authorize(value = SuperUserA.class, message = "Super users A only")
    @GetMapping("super-user-a")
    public void superUserARequest() {
    }

    @Authorize(value = SuperUserB.class, message = "Super users B only")
    @GetMapping("super-user-b")
    public void superUserBRequest() {
    }

    @Authorize(value = {SuperUserA.class, SuperUserB.class}, message = "Super users A or B only")
    @GetMapping("super-users")
    public void superUsersRequest() {
    }

    @Authorize(value = HyperUserA.class, message = "Hyper users A only")
    @GetMapping("hyper-user-a")
    public void hyperUserARequest() {
    }

    @Authorize(value = {HyperUserA.class, SuperUserB.class}, message = "Hyper users A or super users B only")
    @GetMapping("hyper-a-or-super-b")
    public void hyperUserAOrSuperUserBRequest() {
    }
}

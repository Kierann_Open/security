package integration.context;

import fr.open.kierann.security.authentication.AuthenticationConfiguration;
import integration.context.authentication.TestUser;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class TestUserConfiguration implements InitializingBean {
    private final AuthenticationConfiguration configuration;

    @Override
    public void afterPropertiesSet() {
        configuration.registerUserEntityClass(TestUser.class);
    }
}

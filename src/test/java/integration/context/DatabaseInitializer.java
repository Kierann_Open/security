package integration.context;

import integration.context.authentication.TestUser;
import integration.context.authorization.HyperUserA;
import integration.context.authorization.SuperUserA;
import integration.context.authorization.SuperUserB;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Configuration
@Transactional
public class DatabaseInitializer {
    private final PasswordEncoder encoder;

    private final EntityManager entityManager;

    public void initializeAuthenticationData() {
        TestUser user = new TestUser();
        user.setIdentifier("user");
        user.setEncodedPassword(encoder.encode("password"));

        entityManager.persist(user);
    }

    public void initializeAuthorizationData() {
        TestUser user = new TestUser();
        user.setIdentifier("user");
        user.setEncodedPassword(encoder.encode("password"));
        entityManager.persist(user);

        SuperUserA superUserA = new SuperUserA();
        superUserA.setIdentifier("super-user-a");
        superUserA.setEncodedPassword(encoder.encode("password"));
        entityManager.persist(superUserA);

        SuperUserB superUserB = new SuperUserB();
        superUserB.setIdentifier("super-user-b");
        superUserB.setEncodedPassword(encoder.encode("password"));
        entityManager.persist(superUserB);

        HyperUserA hyperUserA = new HyperUserA();
        hyperUserA.setIdentifier("hyper-user-a");
        hyperUserA.setEncodedPassword(encoder.encode("password"));
        entityManager.persist(hyperUserA);
    }
}

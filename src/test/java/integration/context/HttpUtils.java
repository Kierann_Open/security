package integration.context;

import fr.open.kierann.security.authentication.AuthenticationConfiguration;
import lombok.Setter;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.util.TimeValue;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Configuration
public class HttpUtils implements InitializingBean {
    private final TestRestTemplate template = new TestRestTemplate();

    private Set<String> activeAuthenticationTokens = new HashSet<>();

    @Setter
    private String serverPort;

    @Value("${security.authentication.path}/${security.authentication.log-out.mapping}")
    private String logOutPath;

    private UriComponents logOutRequestUri;

    public static void assertErrorMessage(String expectedMessage, ResponseEntity<ErrorResponseBody> response) {
        ErrorResponseBody responseBody = response.getBody();
        assertNotNull(responseBody);
        assertEquals(expectedMessage, responseBody.get("message"));
    }

    public static HttpHeaders createHeadersWithAuthentication(String authenticationToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER, authenticationToken);

        return headers;
    }

    @Override
    public void afterPropertiesSet() {
        template.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory(
                HttpClientBuilder.create().setRetryStrategy(new DefaultHttpRequestRetryStrategy(0, TimeValue.ofSeconds(1))).build()));
        logOutRequestUri = UriComponentsBuilder.fromPath(logOutPath).build();
    }

    public <T> ResponseEntity<T> sendLocalRequest(UriComponents requestComponents, HttpMethod method, Object body, HttpHeaders headers,
                                                  Class<T> responseType) {
        ResponseEntity<T> response = template.exchange("http://localhost:" + serverPort + "/" + requestComponents, method,
                                                       new HttpEntity<>(body, headers), responseType);
        String authenticationToken = response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        if (StringUtils.isNotBlank(authenticationToken)) {
            activeAuthenticationTokens.add(authenticationToken);
        }

        return response;
    }

    public void revokeActiveAuthentications() {
        activeAuthenticationTokens.forEach(this::revokeAuthentication);
        activeAuthenticationTokens = new HashSet<>();
    }

    public void revokeAuthentication(String authenticationToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER, authenticationToken);
        sendLocalRequest(logOutRequestUri, HttpMethod.POST, null, headers, Void.class);
    }
}

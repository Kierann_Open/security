package integration.context.authentication;

import fr.open.kierann.security.authentication.UserEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(of = "id")
@Getter
@Setter
@Entity
@UserEntity(identifier = "identifier", password = "encodedPassword")
public class TestUser {
    @GeneratedValue
    @Id
    private Long id;

    private String identifier;

    private String encodedPassword;
}

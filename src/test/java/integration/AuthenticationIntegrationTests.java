package integration;

import fr.open.kierann.security.authentication.AuthenticationConfiguration;
import integration.context.DatabaseInitializer;
import integration.context.ErrorResponseBody;
import integration.context.HttpUtils;
import integration.context.SecurityTestApplication;
import integration.context.authentication.TestUser;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestConstructor;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static integration.context.HttpUtils.assertErrorMessage;
import static integration.context.HttpUtils.createHeadersWithAuthentication;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RequiredArgsConstructor
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SecurityTestApplication.class)
@DirtiesContext
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
public class AuthenticationIntegrationTests {
    private final DatabaseInitializer data;

    private final HttpUtils http;

    private final HttpHeaders defaultHeaders = new HttpHeaders();

    @LocalServerPort
    private String serverPort;

    @Value("${security.authentication.timeout}")
    private Duration authenticationTimeout;

    @Value("${security.authentication.revocation.timeout}")
    private Duration authenticationRevocationTimeout;

    @Value("${security.authentication.capacity}")
    private Integer authenticationCapacity;

    @Value("${security.authentication.saturation.message}")
    private String authenticationSaturationMessage;

    @Value("${security.authentication.cleanup.min-interval}")
    private Duration authenticationCleanupMinInterval;

    @Value("${security.not-authenticated.message}")
    private String notAuthenticatedMessage;

    @Value("${security.invalid-credentials.message}")
    private String invalidCredentialsMessage;

    @Value("${security.invalid-authentication-token.message}")
    private String invalidAuthenticationTokenMessage;

    @Value("${security.expired-authentication.message}")
    private String expiredAuthenticationMessage;

    @Value("${security.authentication.path}")
    private String authenticationPath;

    @Value("${security.authentication.log-in.mapping}")
    private String logInMapping;

    @Value("${security.authentication.get.mapping}")
    private String getMapping;

    @Value("${security.authentication.renewal.mapping}")
    private String renewalMapping;

    @Value("${security.authentication.log-out.mapping}")
    private String logOutMapping;

    @Value("${security.authentication.identifier.parameter.name}")
    private String identifierParameterName;

    @Value("${security.authentication.raw-password.parameter.name}")
    private String rawPasswordParameterName;

    private UriComponents getRequestUri;

    private UriComponents renewalRequestUri;

    private UriComponents logOutRequestUri;

    @BeforeAll
    public void initialize() {
        defaultHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        defaultHeaders.setContentType(MediaType.APPLICATION_JSON);

        data.initializeAuthenticationData();
        http.setServerPort(serverPort);

        getRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(getMapping).build();
        renewalRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(renewalMapping).build();
        logOutRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logOutMapping).build();
    }

    @BeforeEach
    public void cleanAuthentications() {
        http.revokeActiveAuthentications();
    }

    @Test
    public void handleLogIn_should_authenticate_user_on_valid_credentials() {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();
        assertLoggedIn(logInResponse);
    }

    @Test
    public void handleLogIn_should_fail_on_null_credentials() {
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping).build();
        ResponseEntity<ErrorResponseBody> logInResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertInvalidCredentials(logInResponse);
    }

    @Test
    public void handleLogIn_should_fail_on_null_identifier() {
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(rawPasswordParameterName, "password").build();
        ResponseEntity<ErrorResponseBody> logInResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertInvalidCredentials(logInResponse);
    }

    @Test
    public void handleLogIn_should_fail_on_null_password() {
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(identifierParameterName, "user").build();
        ResponseEntity<ErrorResponseBody> logInResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertInvalidCredentials(logInResponse);
    }

    @Test
    public void handleLogIn_should_fail_on_invalid_identifier() {
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(identifierParameterName, "bad_user")
                                                            .queryParam(rawPasswordParameterName, "password").build();
        ResponseEntity<ErrorResponseBody> logInResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertInvalidCredentials(logInResponse);
    }

    @Test
    public void handleLogIn_should_fail_on_invalid_password() {
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(identifierParameterName, "user")
                                                            .queryParam(rawPasswordParameterName, "bad_password").build();
        ResponseEntity<ErrorResponseBody> logInResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertInvalidCredentials(logInResponse);
    }

    @Test
    public void handleLogIn_should_fail_on_authentication_capacity_saturation_and_authenticate_user_after_authentications_cleanup()
            throws InterruptedException {
        try (ScheduledExecutorService executor = Executors.newScheduledThreadPool(1)) {
            AtomicInteger authenticationCount = new AtomicInteger();
            long period = authenticationTimeout.minus(authenticationCleanupMinInterval).dividedBy(authenticationCapacity).toMillis();
            if (period < 0) {
                period = 1;
            }
            assertLoggedIn(sendNominalLogInRequest());
            executor.scheduleAtFixedRate(() -> {
                if (authenticationCount.get() == authenticationCapacity - 1) {
                    executor.shutdown();
                    return;
                }
                assertLoggedIn(sendNominalLogInRequest());
                authenticationCount.incrementAndGet();
            }, period, period, TimeUnit.MILLISECONDS);

            assertTrue(executor.awaitTermination(20, TimeUnit.SECONDS));
        }

        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(identifierParameterName, "user")
                                                            .queryParam(rawPasswordParameterName, "password").build();
        ResponseEntity<ErrorResponseBody> errorResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE, errorResponse.getStatusCode());
        assertErrorMessage(authenticationSaturationMessage, errorResponse);

        Thread.sleep(authenticationCleanupMinInterval);

        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();
        assertLoggedIn(logInResponse);
    }

    @Test
    public void handleGetAuthenticatedUser_should_return_authenticated_user_after_login() {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        ResponseEntity<TestUser> getUserResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, headers, TestUser.class);

        assertEquals(HttpStatus.OK, getUserResponse.getStatusCode());
        assertEquals(logInResponse.getBody(), getUserResponse.getBody());
    }

    @Test
    public void handleGetAuthenticatedUser_should_fail_on_missing_authentication_token() {
        ResponseEntity<ErrorResponseBody> getUserResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, defaultHeaders,
                                                                                  ErrorResponseBody.class);
        assertNotAuthenticated(getUserResponse);
    }

    @Test
    public void handleGetAuthenticatedUser_should_fail_on_invalid_authentication_token() {
        HttpHeaders headers = createHeadersWithAuthentication("invalid_authentication_token");
        ResponseEntity<ErrorResponseBody> getUserResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, headers,
                                                                                  ErrorResponseBody.class);
        assertNotAuthenticated(getUserResponse);
    }

    @Test
    public void handleGetAuthenticatedUser_should_fail_on_expired_authentication_token() throws InterruptedException {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        Thread.sleep(authenticationTimeout);

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        ResponseEntity<ErrorResponseBody> getUserResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, headers,
                                                                                  ErrorResponseBody.class);
        assertNotAuthenticated(getUserResponse);
    }

    @Test
    public void handleAuthenticationRenewal_should_renew_valid_authentication_token() {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        ResponseEntity<Void> renewalResponse = http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, headers, Void.class);

        String renewedAuthenticationToken = renewalResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.OK, renewalResponse.getStatusCode());
        assertTrue(StringUtils.isNotBlank(renewedAuthenticationToken));
        assertNotEquals(authenticationToken, renewedAuthenticationToken);
    }

    @Test
    public void handleAuthenticationRenewal_should_fail_on_missing_authentication_token() {
        ResponseEntity<ErrorResponseBody> renewalResponse = http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                  ErrorResponseBody.class);

        String renewedAuthenticationToken = renewalResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.UNAUTHORIZED, renewalResponse.getStatusCode());
        assertErrorMessage(invalidAuthenticationTokenMessage, renewalResponse);
        assertNull(renewedAuthenticationToken);
    }

    @Test
    public void handleAuthenticationRenewal_should_fail_on_invalid_authentication_token() {
        HttpHeaders headers = createHeadersWithAuthentication("invalid_authentication_token");
        ResponseEntity<ErrorResponseBody> renewalResponse = http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, headers,
                                                                                  ErrorResponseBody.class);

        String renewedAuthenticationToken = renewalResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.UNAUTHORIZED, renewalResponse.getStatusCode());
        assertErrorMessage(invalidAuthenticationTokenMessage, renewalResponse);
        assertNull(renewedAuthenticationToken);
    }

    @Test
    public void handleAuthenticationRenewal_should_fail_on_expired_authentication_token() throws InterruptedException {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        Thread.sleep(authenticationTimeout);

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        ResponseEntity<ErrorResponseBody> renewalResponse = http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, headers,
                                                                                  ErrorResponseBody.class);

        String renewedAuthenticationToken = renewalResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.UNAUTHORIZED, renewalResponse.getStatusCode());
        assertErrorMessage(expiredAuthenticationMessage, renewalResponse);
        assertNull(renewedAuthenticationToken);
    }

    @Test
    public void handleAuthenticationRenewal_should_revoke_current_authentication_token_on_timeout() throws InterruptedException {
        Duration halfAuthenticationRevocationTimeout = authenticationRevocationTimeout.dividedBy(2);

        ResponseEntity<TestUser> logInRequest = sendNominalLogInRequest();

        String authenticationToken = logInRequest.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, headers, Void.class);

        Thread.sleep(halfAuthenticationRevocationTimeout);

        ResponseEntity<TestUser> nominalResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, headers, TestUser.class);
        assertEquals(HttpStatus.OK, nominalResponse.getStatusCode());
        assertEquals(logInRequest.getBody(), nominalResponse.getBody());

        Thread.sleep(halfAuthenticationRevocationTimeout);

        ResponseEntity<ErrorResponseBody> errorResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, headers,
                                                                                ErrorResponseBody.class);
        assertNotAuthenticated(errorResponse);
    }

    @Test
    public void handleAuthenticationRenewal_should_fail_on_revoked_authentication() {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, headers, Void.class);

        ResponseEntity<ErrorResponseBody> renewalResponse = http.sendLocalRequest(renewalRequestUri, HttpMethod.POST, null, headers,
                                                                                  ErrorResponseBody.class);

        assertEquals(HttpStatus.UNAUTHORIZED, renewalResponse.getStatusCode());
        assertErrorMessage(invalidAuthenticationTokenMessage, renewalResponse);
    }

    @Test
    public void handleLogOut_should_revoke_authentication_on_valid_authentication_token() {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        ResponseEntity<Void> logOutResponse = http.sendLocalRequest(logOutRequestUri, HttpMethod.POST, null, headers, Void.class);

        String authenticationTokenResponse = logOutResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.OK, logOutResponse.getStatusCode());
        assertTrue(StringUtils.isBlank(authenticationTokenResponse));

        ResponseEntity<ErrorResponseBody> getUserResponse = http.sendLocalRequest(getRequestUri, HttpMethod.GET, null, headers,
                                                                                  ErrorResponseBody.class);
        assertNotAuthenticated(getUserResponse);
    }

    @Test
    public void handleLogOut_should_free_up_capacity() {
        for (int authenticationCount = 0; authenticationCount < authenticationCapacity - 1; ++authenticationCount) {
            assertLoggedIn(sendNominalLogInRequest());
        }
        ResponseEntity<TestUser> lastLogInResponse = sendNominalLogInRequest();
        assertLoggedIn(lastLogInResponse);

        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(identifierParameterName, "user")
                                                            .queryParam(rawPasswordParameterName, "password").build();
        ResponseEntity<ErrorResponseBody> errorResponse = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders,
                                                                                ErrorResponseBody.class);
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE, errorResponse.getStatusCode());
        assertErrorMessage(authenticationSaturationMessage, errorResponse);

        String lastAuthenticationToken = lastLogInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(lastAuthenticationToken);
        http.sendLocalRequest(logOutRequestUri, HttpMethod.POST, null, headers, Void.class);

        assertLoggedIn(sendNominalLogInRequest());
    }

    @Test
    public void handleLogOut_should_do_nothing_on_missing_authentication_token() {
        ResponseEntity<Void> logOutResponse = http.sendLocalRequest(logOutRequestUri, HttpMethod.POST, null, defaultHeaders, Void.class);

        String authenticationTokenResponse = logOutResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.OK, logOutResponse.getStatusCode());
        assertTrue(StringUtils.isBlank(authenticationTokenResponse));
    }

    @Test
    public void handleLogOut_should_do_nothing_on_invalid_authentication_token() {
        HttpHeaders headers = createHeadersWithAuthentication("invalid_authentication_token");
        ResponseEntity<Void> logOutResponse = http.sendLocalRequest(logOutRequestUri, HttpMethod.POST, null, headers, Void.class);

        String authenticationTokenResponse = logOutResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.OK, logOutResponse.getStatusCode());
        assertTrue(StringUtils.isBlank(authenticationTokenResponse));
    }

    @Test
    public void handleLogOut_should_do_nothing_on_expired_authentication_token() throws InterruptedException {
        ResponseEntity<TestUser> logInResponse = sendNominalLogInRequest();

        Thread.sleep(authenticationTimeout);

        String authenticationToken = logInResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        HttpHeaders headers = createHeadersWithAuthentication(authenticationToken);
        ResponseEntity<Void> logOutResponse = http.sendLocalRequest(logOutRequestUri, HttpMethod.POST, null, headers, Void.class);

        String authenticationTokenResponse = logOutResponse.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(HttpStatus.OK, logOutResponse.getStatusCode());
        assertTrue(StringUtils.isBlank(authenticationTokenResponse));
    }

    private ResponseEntity<TestUser> sendNominalLogInRequest() {
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(authenticationPath).pathSegment(logInMapping)
                                                            .queryParam(identifierParameterName, "user")
                                                            .queryParam(rawPasswordParameterName, "password").build();
        return http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, defaultHeaders, TestUser.class);
    }

    private void assertLoggedIn(ResponseEntity<?> response) {
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(StringUtils.isNotBlank(response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER)));
        assertNotNull(response.getBody());
    }

    private void assertInvalidCredentials(ResponseEntity<ErrorResponseBody> response) {
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertNull(response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER));
        assertErrorMessage(invalidCredentialsMessage, response);
    }

    private void assertNotAuthenticated(ResponseEntity<ErrorResponseBody> response) {
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertErrorMessage(notAuthenticatedMessage, response);
    }
}

package integration;

import fr.open.kierann.security.authentication.AuthenticationConfiguration;
import integration.context.DatabaseInitializer;
import integration.context.ErrorResponseBody;
import integration.context.HttpUtils;
import integration.context.SecurityTestApplication;
import integration.context.authentication.TestUser;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestConstructor;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

import static integration.context.HttpUtils.assertErrorMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RequiredArgsConstructor
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SecurityTestApplication.class)
@DirtiesContext
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
public class AuthorizationIntegrationTests {
    private final DatabaseInitializer data;

    private final HttpUtils http;

    @LocalServerPort
    private String serverPort;

    @Value("${security.authentication.path}/${security.authentication.log-in.mapping}")
    private String logInPath;

    @Value("${security.authentication.identifier.parameter.name}")
    private String identifierParameterName;

    @Value("${security.authentication.raw-password.parameter.name}")
    private String rawPasswordParameterName;

    @BeforeAll
    public void initialize() {
        data.initializeAuthorizationData();
        http.setServerPort(serverPort);
    }

    @AfterEach
    public void afterEach() {
        http.revokeActiveAuthentications();
    }

    @Test
    public void publicRequest_should_accept_any_request() {
        String requestPath = "public";
        assertOk(requestPath, null);
        assertOk(requestPath, "user");
        assertOk(requestPath, "super-user-a");
        assertOk(requestPath, "super-user-b");
        assertOk(requestPath, "hyper-user-a");
    }

    @Test
    public void userRequest_should_accept_user_authenticated_request_only() {
        String requestPath = "user";
        String forbiddenMessage = "Users only";
        assertForbidden(requestPath, null, forbiddenMessage);
        assertOk(requestPath, "user");
        assertOk(requestPath, "super-user-a");
        assertOk(requestPath, "super-user-b");
        assertOk(requestPath, "hyper-user-a");
    }

    @Test
    public void superUserARequest_should_accept_superUserA_authenticated_request_only() {
        String requestPath = "super-user-a";
        String forbiddenMessage = "Super users A only";
        assertForbidden(requestPath, null, forbiddenMessage);
        assertForbidden(requestPath, "user", forbiddenMessage);
        assertOk(requestPath, "super-user-a");
        assertForbidden(requestPath, "super-user-b", forbiddenMessage);
        assertOk(requestPath, "hyper-user-a");
    }

    @Test
    public void superUserBRequest_should_accept_superUserB_authenticated_request_only() {
        String requestPath = "super-user-b";
        String forbiddenMessage = "Super users B only";
        assertForbidden(requestPath, null, forbiddenMessage);
        assertForbidden(requestPath, "user", forbiddenMessage);
        assertForbidden(requestPath, "super-user-a", forbiddenMessage);
        assertOk(requestPath, "super-user-b");
        assertForbidden(requestPath, "hyper-user-a", forbiddenMessage);
    }

    @Test
    public void superUsersRequest_should_accept_superUsers_authenticated_request_only() {
        String requestPath = "super-users";
        String forbiddenMessage = "Super users A or B only";
        assertForbidden(requestPath, null, forbiddenMessage);
        assertForbidden(requestPath, "user", forbiddenMessage);
        assertOk(requestPath, "super-user-a");
        assertOk(requestPath, "super-user-b");
        assertOk(requestPath, "hyper-user-a");
    }

    @Test
    public void hyperUserARequest_should_accept_hyperUserA_authenticated_request_only() {
        String requestPath = "hyper-user-a";
        String forbiddenMessage = "Hyper users A only";
        assertForbidden(requestPath, null, forbiddenMessage);
        assertForbidden(requestPath, "user", forbiddenMessage);
        assertForbidden(requestPath, "super-user-a", forbiddenMessage);
        assertForbidden(requestPath, "super-user-b", forbiddenMessage);
        assertOk(requestPath, "hyper-user-a");
    }

    @Test
    public void hyperUserAOrSuperUserBRequest_should_accept_hyperUserA_or_superUserB_authenticated_request_only() {
        String requestPath = "hyper-a-or-super-b";
        String forbiddenMessage = "Hyper users A or super users B only";
        assertForbidden(requestPath, null, forbiddenMessage);
        assertForbidden(requestPath, "user", forbiddenMessage);
        assertForbidden(requestPath, "super-user-a", forbiddenMessage);
        assertOk(requestPath, "super-user-b");
        assertOk(requestPath, "hyper-user-a");
    }

    private void assertOk(String requestPath, String userIdentifier) {
        ResponseEntity<Void> response = getResponse(requestPath, userIdentifier, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private void assertForbidden(String requestPath, String userIdentifier, String expectedMessage) {
        ResponseEntity<ErrorResponseBody> response = getErrorResponse(requestPath, userIdentifier);
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertErrorMessage(expectedMessage, response);
    }

    private ResponseEntity<ErrorResponseBody> getErrorResponse(String requestPath, String userIdentifier) {
        return getResponse(requestPath, userIdentifier, ErrorResponseBody.class);
    }

    private <T> ResponseEntity<T> getResponse(String requestPath, String userIdentifier, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String authenticationToken = null;
        if (userIdentifier != null) {
            authenticationToken = getAuthenticationToken(userIdentifier);
            headers.set(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER, authenticationToken);
        }

        UriComponents requestUri = UriComponentsBuilder.fromPath("fr.open.security.authorization-test").pathSegment(requestPath).build();
        ResponseEntity<T> response = http.sendLocalRequest(requestUri, HttpMethod.GET, null, headers, responseType);
        if (authenticationToken != null) {
            http.revokeAuthentication(authenticationToken);
        }
        return response;
    }

    private String getAuthenticationToken(String identifier) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        UriComponents logInRequestUri = UriComponentsBuilder.fromPath(logInPath).queryParam(identifierParameterName, identifier)
                                                            .queryParam(rawPasswordParameterName, "password").build();
        ResponseEntity<TestUser> response = http.sendLocalRequest(logInRequestUri, HttpMethod.POST, null, headers, TestUser.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        return response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
    }
}

package fr.open.kierann.security.authentication;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnitUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class UserResolverTests {
    @Mock
    private AuthenticationConfiguration configuration;

    @Mock
    private EntityManager entityManager;

    @Mock
    private PersistenceUnitUtil persistenceUnitUtil;

    @InjectMocks
    private UserResolver userResolver;

    @BeforeEach
    public void initializeComponent(@Mock EntityManagerFactory factoryMock) {
        doReturn(factoryMock).when(entityManager).getEntityManagerFactory();
        doReturn(persistenceUnitUtil).when(factoryMock).getPersistenceUnitUtil();
        doReturn(TestUser.class).when(configuration).getUserEntity();
        userResolver.afterPropertiesSet();
    }

    @Test
    public void getUserId_should_return_user_id_on_valid_user_entity(@Mock Object expectedUserId, @Mock TestUser user) {
        doReturn(expectedUserId).when(persistenceUnitUtil).getIdentifier(eq(user));

        assertEquals(expectedUserId, userResolver.getUserId(user));
    }

    @Test
    public void getUser_should_return_user_on_valid_authentication(@Mock Object userId, @Mock TestUser expectedUser) {
        doReturn(expectedUser).when(entityManager).find(eq(TestUser.class), eq(userId));

        assertEquals(expectedUser, userResolver.getUser(userId));
    }

    @Test
    public void getUser_should_throw_on_invalid_authentication(@Mock Object userId) {
        doReturn(null).when(entityManager).find(eq(TestUser.class), eq(userId));

        assertThrows(IllegalArgumentException.class, () -> userResolver.getUser(userId));
    }

    private static class TestUser {
    }
}

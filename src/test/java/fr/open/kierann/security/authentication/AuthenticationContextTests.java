package fr.open.kierann.security.authentication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class AuthenticationContextTests {
    private final String notAuthenticatedMessage = "Not authenticated-test";

    @InjectMocks
    private AuthenticationContext<Object> authenticationContext;

    @BeforeEach
    public void configureComponent() {
        authenticationContext = new AuthenticationContext<>();
        ReflectionTestUtils.setField(authenticationContext, "notAuthenticatedMessage", notAuthenticatedMessage);
    }

    @Test
    public void setUser_should_configure_user_once(@Mock Object user) {
        authenticationContext.setUser(user);
        assertEquals(user, authenticationContext.getUser());
        assertThrows(IllegalStateException.class, () -> authenticationContext.setUser(user));
    }

    @Test
    public void findUser_should_return_optional_user(@Mock Object user) {
        assertTrue(authenticationContext.findUser().isEmpty());
        authenticationContext.setUser(user);
        assertTrue(authenticationContext.findUser().isPresent());
    }

    @Test
    public void getUser_should_return_user_if_present_and_throw_if_absent(@Mock Object user) {
        assertThrows(AuthenticationException.class, () -> authenticationContext.getUser(), notAuthenticatedMessage);
        authenticationContext.setUser(user);
        assertEquals(user, authenticationContext.getUser());
    }
}

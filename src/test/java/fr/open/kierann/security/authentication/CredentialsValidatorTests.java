package fr.open.kierann.security.authentication;

import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
public class CredentialsValidatorTests {
    private final Class<?> userEntity = TestUser.class;

    private final Field userEntityIdentifierField = userEntity.getDeclaredField("identifier");

    private final Field userEntityPasswordField = userEntity.getDeclaredField("encodedPassword");

    @Mock
    AuthenticationConfiguration configuration;

    @Mock
    private EntityManager entityManager;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private CredentialsValidator credentialsValidator;

    public CredentialsValidatorTests() throws NoSuchFieldException {
        userEntityPasswordField.setAccessible(true);
    }

    @BeforeEach
    public void initializeComponent() {
        doReturn(TestUser.class).when(configuration).getUserEntity();
        doReturn(userEntityIdentifierField).when(configuration).getUserEntityIdentifierField();
        credentialsValidator.afterPropertiesSet();
    }

    @Test
    public void validate_should_return_user_on_valid_credentials(@Mock TypedQuery<TestUser> userQueryMock) {
        TestUser expectedUser = new TestUser();
        String userIdentifier = "user";
        String userPassword = "password";

        doReturn(userQueryMock).when(entityManager).createQuery(anyString(), eq(TestUser.class));
        doReturn(userQueryMock).when(userQueryMock).setParameter(anyString(), eq(userIdentifier));
        doReturn(expectedUser).when(userQueryMock).getSingleResult();

        doReturn(userEntityPasswordField).when(configuration).getUserEntityPasswordField();
        doReturn(true).when(passwordEncoder).matches(eq(userPassword), any());

        Object returnedUser = credentialsValidator.validate(userIdentifier, userPassword);

        assertEquals(expectedUser, returnedUser);
    }

    @Test
    public void validate_should_throw_on_null_credentials() {
        String userIdentifier = "user";
        String userPassword = "password";

        assertThrows(AuthenticationException.class, () -> credentialsValidator.validate(null, null));

        assertThrows(AuthenticationException.class, () -> credentialsValidator.validate(userIdentifier, null));

        assertThrows(AuthenticationException.class, () -> credentialsValidator.validate(null, userPassword));
    }

    @Test
    public void validate_should_throw_on_invalid_identifier(@Mock TypedQuery<TestUser> userQueryMock) {
        String userIdentifier = "bad_user";
        String userPassword = "password";

        doReturn(userQueryMock).when(entityManager).createQuery(anyString(), eq(TestUser.class));
        doReturn(userQueryMock).when(userQueryMock).setParameter(anyString(), eq(userIdentifier));
        doThrow(NoResultException.class).when(userQueryMock).getSingleResult();

        assertThrows(AuthenticationException.class, () -> credentialsValidator.validate(userIdentifier, userPassword));
    }

    @Test
    public void validate_should_throw_on_invalid_password(@Mock TypedQuery<TestUser> userQueryMock) {
        TestUser user = new TestUser();
        String userIdentifier = "user";
        String userPassword = "bad_password";

        doReturn(userQueryMock).when(entityManager).createQuery(anyString(), eq(TestUser.class));
        doReturn(userQueryMock).when(userQueryMock).setParameter(anyString(), eq(userIdentifier));
        doReturn(user).when(userQueryMock).getSingleResult();

        doReturn(userEntityPasswordField).when(configuration).getUserEntityPasswordField();
        doReturn(false).when(passwordEncoder).matches(eq(userPassword), any());

        assertThrows(AuthenticationException.class, () -> credentialsValidator.validate(userIdentifier, userPassword));
    }

    @Getter
    @Setter
    private static class TestUser {
        private String identifier;

        private String encodedPassword;
    }
}

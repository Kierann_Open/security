package fr.open.kierann.security.authentication;

import jakarta.persistence.EntityManager;
import jakarta.persistence.metamodel.EntityType;
import jakarta.persistence.metamodel.Metamodel;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.core.annotation.AnnotationConfigurationException;

import java.lang.reflect.Field;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class AuthenticationConfigurationTests {
    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private AuthenticationConfiguration configuration;

    @Test
    public void afterPropertiesSet_should_configure_first_annotated_user_entity(@Mock Metamodel metamodel,
                                                                                @Mock EntityType<TestUserWithoutAnnotation> testUserWithoutAnnotationEntity,
                                                                                @Mock EntityType<NominalTestUser> nominalTestUserEntity,
                                                                                @Mock NominalTestUser user) {
        doReturn(metamodel).when(entityManager).getMetamodel();
        doReturn(Set.of(testUserWithoutAnnotationEntity, nominalTestUserEntity)).when(metamodel).getEntities();
        lenient().doReturn(TestUserWithoutAnnotation.class).when(testUserWithoutAnnotationEntity).getJavaType();
        doReturn(NominalTestUser.class).when(nominalTestUserEntity).getJavaType();

        configuration.afterPropertiesSet();
        assertEquals(NominalTestUser.class, configuration.getUserEntity());
        assertNotNull(configuration.getUserEntityIdentifierField());
        Field passwordField = configuration.getUserEntityPasswordField();
        assertNotNull(passwordField);
        assertDoesNotThrow(() -> passwordField.get(user));
    }

    @Test
    public void afterPropertiesSet_should_throw_when_no_annotated_user_entity_found(@Mock Metamodel metamodel,
                                                                                    @Mock EntityType<TestUserWithoutAnnotation> testUserWithoutAnnotationEntity) {
        doReturn(metamodel).when(entityManager).getMetamodel();
        doReturn(Set.of(testUserWithoutAnnotationEntity)).when(metamodel).getEntities();
        doReturn(TestUserWithoutAnnotation.class).when(testUserWithoutAnnotationEntity).getJavaType();

        assertThrows(BeanInitializationException.class, () -> configuration.afterPropertiesSet());
    }

    @Test
    public void registerUserEntityClass_should_configure_user_entity_on_valid_configuration(@Mock NominalTestUser user) {
        configuration.registerUserEntityClass(NominalTestUser.class);
        assertEquals(NominalTestUser.class, configuration.getUserEntity());
        assertNotNull(configuration.getUserEntityIdentifierField());
        Field passwordField = configuration.getUserEntityPasswordField();
        assertNotNull(passwordField);
        assertDoesNotThrow(() -> passwordField.get(user));
    }

    @Test
    public void registerUserEntityClass_should_throw_on_invalid_configuration() {
        assertThrows(AnnotationConfigurationException.class, () -> configuration.registerUserEntityClass(TestUserWithoutAnnotation.class));
        assertThrows(AnnotationConfigurationException.class, () -> configuration.registerUserEntityClass(TestUserWithInvalidIdentifier.class));
        assertThrows(AnnotationConfigurationException.class, () -> configuration.registerUserEntityClass(TestUserWithInvalidPassword.class));
    }

    @Getter
    @Setter
    private static class TestUserWithoutAnnotation {
        private String identifier;

        private String password;
    }

    @Getter
    @Setter
    @UserEntity(identifier = "identifier", password = "password")
    private static class NominalTestUser {
        private String identifier;

        private String password;
    }

    @Getter
    @Setter
    @UserEntity(identifier = "bad_identifier", password = "password")
    private static class TestUserWithInvalidIdentifier {
        private String identifier;

        private String password;
    }

    @Getter
    @Setter
    @UserEntity(identifier = "identifier", password = "bad_password")
    private static class TestUserWithInvalidPassword {
        private String identifier;

        private String password;
    }
}

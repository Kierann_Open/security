package fr.open.kierann.security.authentication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class AuthenticationManagerTests {
    private final Integer authenticationTokenLength = 10;

    private final Duration authenticationTimeout = Duration.ofMillis(200);

    private final Duration authenticationRevocationTimeout = Duration.ofMillis(100);

    private final Integer authenticationCapacity = 3;

    private final Duration authenticationCleanupMinInterval = Duration.ofMillis(100);

    private final String authenticationSaturationMessage = "Saturation-test";

    private final String invalidAuthenticationTokenMessage = "Invalid fr.open.security.authentication token-test";

    private final String expiredAuthenticationMessage = "Expired fr.open.security.authentication-test";

    @Mock
    private UserResolver userResolver;

    private AuthenticationManager authenticationManager;

    @BeforeEach
    public void configureService() {
        authenticationManager = new AuthenticationManager(userResolver);
        ReflectionTestUtils.setField(authenticationManager, "authenticationTokenLength", authenticationTokenLength);
        ReflectionTestUtils.setField(authenticationManager, "authenticationTimeout", authenticationTimeout);
        ReflectionTestUtils.setField(authenticationManager, "authenticationRevocationTimeout", authenticationRevocationTimeout);
        ReflectionTestUtils.setField(authenticationManager, "authenticationCapacity", authenticationCapacity);
        ReflectionTestUtils.setField(authenticationManager, "authenticationCleanupMinInterval", authenticationCleanupMinInterval);
        ReflectionTestUtils.setField(authenticationManager, "authenticationSaturationMessage", authenticationSaturationMessage);
        ReflectionTestUtils.setField(authenticationManager, "invalidAuthenticationTokenMessage", invalidAuthenticationTokenMessage);
        ReflectionTestUtils.setField(authenticationManager, "expiredAuthenticationMessage", expiredAuthenticationMessage);
    }

    @Test
    public void getAuthenticatedUser_should_return_user_on_valid_authentication_token(@Mock Object expectedUser) {
        String authenticationToken = authenticationManager.authenticate(expectedUser);

        doReturn(expectedUser).when(userResolver).getUser(any());

        assertEquals(expectedUser, authenticationManager.getAuthenticatedUser(authenticationToken));
    }

    @Test
    public void getAuthenticatedUser_should_throw_on_invalid_authentication_token() {
        assertThrows(AuthenticationException.class, () -> authenticationManager.getAuthenticatedUser(null), invalidAuthenticationTokenMessage);

        String invalidAuthenticationToken = "invalid-authentication-token";
        assertThrows(AuthenticationException.class, () -> authenticationManager.getAuthenticatedUser(invalidAuthenticationToken),
                     invalidAuthenticationTokenMessage);
    }

    @Test
    public void getAuthenticatedUser_should_throw_on_expired_authentication_token(@Mock Object user) throws InterruptedException {
        String authenticationToken = authenticationManager.authenticate(user);

        Thread.sleep(authenticationTimeout);

        assertThrows(AuthenticationException.class, () -> authenticationManager.getAuthenticatedUser(authenticationToken),
                     expiredAuthenticationMessage);
    }

    @Test
    public void authenticate_should_return_valid_authentication_token_on_user_which_expires_as_expected(@Mock Object user, @Mock Object userId)
            throws InterruptedException {
        Duration halfAuthenticationTimeout = authenticationTimeout.dividedBy(2);

        doReturn(userId).when(userResolver).getUserId(user);
        String authenticationToken = authenticationManager.authenticate(user);
        assertValid(authenticationToken);

        Thread.sleep(halfAuthenticationTimeout);

        assertDoesNotThrow(() -> authenticationManager.getAuthenticatedUser(authenticationToken));

        Thread.sleep(halfAuthenticationTimeout);

        assertThrows(AuthenticationException.class, () -> authenticationManager.getAuthenticatedUser(authenticationToken),
                     expiredAuthenticationMessage);
    }

    @Test
    public void authenticate_should_throw_on_saturation_and_authenticate_after_cleaning(@Mock Object user) throws InterruptedException {
        try (ScheduledExecutorService executor = Executors.newScheduledThreadPool(1)) {
            AtomicInteger authenticationCount = new AtomicInteger();
            long period = authenticationTimeout.minus(authenticationCleanupMinInterval).dividedBy(authenticationCapacity).toMillis();
            if (period < 0) {
                period = 1;
            }
            assertValid(authenticationManager.authenticate(user));
            executor.scheduleAtFixedRate(() -> {
                if (authenticationCount.get() == authenticationCapacity - 1) {
                    executor.shutdown();
                    return;
                }
                assertValid(authenticationManager.authenticate(user));
                authenticationCount.incrementAndGet();
            }, period, period, TimeUnit.MILLISECONDS);

            assertTrue(executor.awaitTermination(10, TimeUnit.SECONDS));
        }

        assertThrows(SaturationException.class, () -> authenticationManager.authenticate(user), authenticationSaturationMessage);

        Thread.sleep(authenticationCleanupMinInterval);

        assertValid(authenticationManager.authenticate(user));
    }

    @Test
    public void renewAuthentication_should_renew_valid_authentication_token(@Mock Object user) {
        String authenticationToken = authenticationManager.authenticate(user);

        String renewedAuthenticationToken = authenticationManager.renewAuthentication(authenticationToken);
        assertValid(renewedAuthenticationToken);
        assertNotEquals(authenticationToken, renewedAuthenticationToken);
    }

    @Test
    public void renewAuthentication_should_throw_on_invalid_authentication_token() {
        assertThrows(AuthenticationException.class, () -> authenticationManager.renewAuthentication(null), invalidAuthenticationTokenMessage);

        String invalidAuthenticationToken = "invalid-authentication-token";
        assertThrows(AuthenticationException.class, () -> authenticationManager.renewAuthentication(invalidAuthenticationToken),
                     invalidAuthenticationTokenMessage);
    }

    @Test
    public void renewAuthentication_should_throw_on_revoked_authentication_token(@Mock Object user) {
        String authenticationToken = authenticationManager.authenticate(user);

        authenticationManager.renewAuthentication(authenticationToken);

        assertThrows(AuthenticationException.class, () -> authenticationManager.renewAuthentication(authenticationToken),
                     invalidAuthenticationTokenMessage);
    }

    @Test
    public void renewAuthentication_should_throw_on_expired_authentication_token(@Mock Object user) throws InterruptedException {
        String authenticationToken = authenticationManager.authenticate(user);

        Thread.sleep(authenticationTimeout);

        assertThrows(AuthenticationException.class, () -> authenticationManager.renewAuthentication(authenticationToken),
                     expiredAuthenticationMessage);
    }

    @Test
    public void revokeAuthentication_should_revoke_existing_authentication(@Mock Object user) {
        String authenticationToken = authenticationManager.authenticate(user);

        assertDoesNotThrow(() -> authenticationManager.revokeAuthentication(authenticationToken));

        assertThrows(AuthenticationException.class, () -> authenticationManager.getAuthenticatedUser(authenticationToken),
                     invalidAuthenticationTokenMessage);
    }

    @Test
    public void revokeAuthentication_should_free_up_capacity(@Mock Object user) {
        for (int authenticationCount = 0; authenticationCount < authenticationCapacity - 1; ++authenticationCount) {
            assertValid(authenticationManager.authenticate(user));
        }
        String lastAuthenticationToken = authenticationManager.authenticate(user);
        assertValid(lastAuthenticationToken);

        assertThrows(SaturationException.class, () -> authenticationManager.authenticate(user), authenticationSaturationMessage);

        assertDoesNotThrow(() -> authenticationManager.revokeAuthentication(lastAuthenticationToken));

        assertValid(authenticationManager.authenticate(user));
    }

    @Test
    public void revokeAuthentication_should_do_nothing_on_invalid_or_null_authentication() {
        String invalidAuthenticationToken = "invalid-token";
        assertDoesNotThrow(() -> authenticationManager.revokeAuthentication(invalidAuthenticationToken));

        assertDoesNotThrow(() -> authenticationManager.revokeAuthentication(null));
    }

    @Test
    public void revokeAuthentication_should_do_nothing_on_expired_authentication(@Mock Object user) throws InterruptedException {
        String authenticationToken = authenticationManager.authenticate(user);

        Thread.sleep(authenticationTimeout);

        assertDoesNotThrow(() -> authenticationManager.revokeAuthentication(authenticationToken));
    }

    private void assertValid(String authenticationToken) {
        assertNotNull(authenticationToken);
        assertEquals(authenticationTokenLength, authenticationToken.length());
        assertTrue(authenticationToken.matches("^[a-zA-Z\\d]*$"));
    }
}

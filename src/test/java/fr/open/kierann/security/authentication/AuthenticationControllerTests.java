package fr.open.kierann.security.authentication;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AuthenticationControllerTests {
    @Mock
    private CredentialsValidator credentialsValidator;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private AuthenticationContext<Object> authenticationContext;

    @InjectMocks
    private AuthenticationController authenticationController;

    @Test
    public void handleLogIn_should_authenticate_user_on_valid_credentials(@Mock Object expectedUser) {
        String identifier = "user";
        String rawPassword = "password";
        String expectedAuthenticationToken = "token";

        doReturn(expectedUser).when(credentialsValidator).validate(eq(identifier), eq(rawPassword));
        doReturn(expectedAuthenticationToken).when(authenticationManager).authenticate(eq(expectedUser));

        ResponseEntity<Object> response = authenticationController.handleLogIn(identifier, rawPassword);
        assertEquals(expectedUser, response.getBody());
        String responseAuthenticationToken = response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(expectedAuthenticationToken, responseAuthenticationToken);
    }

    @Test
    public void handleLogIn_should_throw_on_invalid_credentials() {
        String identifier = "bad_user";
        String rawPassword = "bad_password";

        doThrow(AuthenticationException.class).when(credentialsValidator).validate(eq(identifier), eq(rawPassword));

        assertThrows(AuthenticationException.class, () -> authenticationController.handleLogIn(identifier, rawPassword));
    }

    @Test
    public void handleGetAuthenticatedUser_should_return_authenticated_user_on_valid_authentication(@Mock Object expectedUser) {
        doReturn(expectedUser).when(authenticationContext).getUser();

        assertEquals(expectedUser, authenticationController.handleGetAuthenticatedUser());
    }

    @Test
    public void handleGetAuthenticatedUser_should_throw_on_invalid_authentication() {
        doThrow(AuthenticationException.class).when(authenticationContext).getUser();

        assertThrows(AuthenticationException.class, () -> authenticationController.handleGetAuthenticatedUser());
    }

    @Test
    public void handleAuthenticationRenewal_should_renew_authentication_on_valid_authentication(@Mock RequestEntity<Void> request,
                                                                                                @Mock HttpHeaders requestHeaders) {
        String authenticationToken = "token";
        String renewedAuthenticationToken = "new-token";

        doReturn(authenticationToken).when(requestHeaders).getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        doReturn(requestHeaders).when(request).getHeaders();
        doReturn(renewedAuthenticationToken).when(authenticationManager).renewAuthentication(eq(authenticationToken));

        ResponseEntity<Void> response = authenticationController.handleAuthenticationRenewal(request);

        String responseAuthenticationToken = response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertEquals(renewedAuthenticationToken, responseAuthenticationToken);
    }

    @Test
    public void handleAuthenticationRenewal_should_throw_on_invalid_authentication(@Mock RequestEntity<Void> request,
                                                                                   @Mock HttpHeaders requestHeaders) {
        doReturn(requestHeaders).when(request).getHeaders();
        doThrow(AuthenticationException.class).when(authenticationManager).renewAuthentication(any());

        assertThrows(AuthenticationException.class, () -> authenticationController.handleAuthenticationRenewal(request));
    }

    @Test
    public void handleLogOut_should_try_authentication_revocation(@Mock RequestEntity<Void> request, @Mock HttpHeaders requestHeaders) {
        String authenticationToken = "token";

        doReturn(authenticationToken).when(requestHeaders).getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        doReturn(requestHeaders).when(request).getHeaders();

        ResponseEntity<Void> response = authenticationController.handleLogOut(request);

        verify(authenticationManager).revokeAuthentication(eq(authenticationToken));

        String responseAuthenticationToken = response.getHeaders().getFirst(AuthenticationConfiguration.AUTHENTICATION_TOKEN_HEADER);
        assertNotNull(responseAuthenticationToken);
        assertTrue(responseAuthenticationToken.isBlank());
    }
}

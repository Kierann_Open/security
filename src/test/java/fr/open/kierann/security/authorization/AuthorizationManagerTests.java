package fr.open.kierann.security.authorization;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class AuthorizationManagerTests {
    @Mock
    private BaseTestUser baseUser;

    @Mock
    private SuperTestUserA superUserA;

    @Mock
    private SuperTestUserB superUserB;

    @Mock
    private HyperTestUserA hyperUserA;

    @InjectMocks
    private AuthorizationManager authorizationManager;

    @Test
    public void authorize_should_authorize_anyone() {
        assertDoesNotThrow(() -> authorizationManager.authorize(null, null));

        assertDoesNotThrow(() -> authorizationManager.authorize(baseUser, null));

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserA, null));

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserB, null));

        assertDoesNotThrow(() -> authorizationManager.authorize(hyperUserA, null));
    }

    @Test
    public void authorize_should_authorize_users_only(@Mock Authorize authorize) {
        String errorMessage = "Users only";

        doReturn(new Class<?>[]{BaseTestUser.class}).when(authorize).value();
        doReturn(errorMessage).when(authorize).message();

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(null, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(baseUser, authorize));

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserA, authorize));

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserB, authorize));

        assertDoesNotThrow(() -> authorizationManager.authorize(hyperUserA, authorize));
    }

    @Test
    public void authorize_should_authorize_super_users_a_only(@Mock Authorize authorize) {
        String errorMessage = "Super users A only";

        doReturn(new Class<?>[]{SuperTestUserA.class}).when(authorize).value();
        doReturn(errorMessage).when(authorize).message();

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(null, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(baseUser, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserA, authorize));

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(superUserB, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(hyperUserA, authorize));
    }

    @Test
    public void authorize_should_authorize_super_users_b_only(@Mock Authorize authorize) {
        String errorMessage = "Super users B only";

        doReturn(new Class<?>[]{SuperTestUserB.class}).when(authorize).value();
        doReturn(errorMessage).when(authorize).message();

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(null, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(baseUser, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(superUserA, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserB, authorize));

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(hyperUserA, authorize), errorMessage);
    }

    @Test
    public void authorize_should_authorize_super_users_a_or_b_only(@Mock Authorize authorize) {
        String errorMessage = "Super users A or B only";

        doReturn(new Class<?>[]{SuperTestUserA.class, SuperTestUserB.class}).when(authorize).value();
        doReturn(errorMessage).when(authorize).message();

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(null, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(baseUser, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserA, authorize));

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserB, authorize));

        assertDoesNotThrow(() -> authorizationManager.authorize(hyperUserA, authorize));
    }

    @Test
    public void authorize_should_authorize_hyper_users_a_only(@Mock Authorize authorize) {
        String errorMessage = "Hyper users A only";

        doReturn(new Class<?>[]{HyperTestUserA.class}).when(authorize).value();
        doReturn(errorMessage).when(authorize).message();

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(null, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(baseUser, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(superUserA, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(superUserB, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(hyperUserA, authorize));
    }

    @Test
    public void authorize_should_authorize_hyper_users_a_or_super_users_b_only(@Mock Authorize authorize) {
        String errorMessage = "Hyper users A or super users B only";

        doReturn(new Class<?>[]{HyperTestUserA.class, SuperTestUserB.class}).when(authorize).value();
        doReturn(errorMessage).when(authorize).message();

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(null, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(baseUser, authorize), errorMessage);

        assertThrows(AuthorizationException.class, () -> authorizationManager.authorize(superUserA, authorize), errorMessage);

        assertDoesNotThrow(() -> authorizationManager.authorize(superUserB, authorize));

        assertDoesNotThrow(() -> authorizationManager.authorize(hyperUserA, authorize));
    }

    private static class BaseTestUser {
    }

    private static class SuperTestUserA extends BaseTestUser {
    }

    private static class SuperTestUserB extends BaseTestUser {
    }

    private static class HyperTestUserA extends SuperTestUserA {
    }
}

package fr.open.kierann.security;

import fr.open.kierann.security.authentication.AuthenticationContext;
import fr.open.kierann.security.authentication.AuthenticationException;
import fr.open.kierann.security.authorization.AuthorizationException;
import fr.open.kierann.security.authentication.AuthenticationManager;
import fr.open.kierann.security.authorization.AuthorizationManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class SecurityInterceptorTests {
    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private AuthorizationManager authorizationManager;

    @Mock
    private AuthenticationContext<Object> authenticationContext;

    @InjectMocks
    private SecurityInterceptor securityInterceptor;

    @Test
    public void preHandle_should_authenticate_user_on_public_request_and_valid_authentication_token(@Mock HttpServletRequest request,
                                                                                                    @Mock HttpServletResponse response,
                                                                                                    @Mock HandlerMethod handler, @Mock Object user) {
        doReturn(user).when(authenticationManager).getAuthenticatedUser(any());

        assertTrue(securityInterceptor.preHandle(request, response, handler));
        verify(authenticationContext).setUser(eq(user));
    }

    @Test
    public void preHandle_should_do_nothing_on_public_request_and_invalid_authentication_token(@Mock HttpServletRequest request,
                                                                                               @Mock HttpServletResponse response,
                                                                                               @Mock HandlerMethod handler) {
        Mockito.doThrow(AuthenticationException.class).when(authenticationManager).getAuthenticatedUser(any());

        assertTrue(securityInterceptor.preHandle(request, response, handler));
        verify(authenticationContext, times(0)).setUser(any());
    }

    @Test
    public void preHandle_should_return_true_on_authorized_request_and_authorized_user(@Mock HttpServletRequest request,
                                                                                       @Mock HttpServletResponse response,
                                                                                       @Mock HandlerMethod handler, @Mock Object user) {
        doReturn(user).when(authenticationManager).getAuthenticatedUser(any());
        doNothing().when(authorizationManager).authorize(any(), any());

        assertTrue(securityInterceptor.preHandle(request, response, handler));
    }

    @Test
    public void preHandle_should_throw_on_authorized_request_and_unauthorized_user(@Mock HttpServletRequest request,
                                                                                   @Mock HttpServletResponse response, @Mock HandlerMethod handler,
                                                                                   @Mock Object user) {
        doReturn(user).when(authenticationManager).getAuthenticatedUser(any());
        Mockito.doThrow(AuthorizationException.class).when(authorizationManager).authorize(eq(user), any());

        assertThrows(AuthorizationException.class, () -> securityInterceptor.preHandle(request, response, handler));
    }

    @Test
    public void addInterceptors_should_register_self_and_exclude_error_paths(@Mock InterceptorRegistry interceptorRegistry,
                                                                             @Mock InterceptorRegistration interceptorRegistration) {
        doReturn(interceptorRegistration).when(interceptorRegistry).addInterceptor(any());
        securityInterceptor.addInterceptors(interceptorRegistry);
        verify(interceptorRegistry).addInterceptor(eq(securityInterceptor));
        verify(interceptorRegistration).excludePathPatterns(eq("/error/**"));
    }
}
